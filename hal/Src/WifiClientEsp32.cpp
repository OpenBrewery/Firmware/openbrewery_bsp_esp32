//
// Created by mchodzikiewicz on 2/3/19.
//

#include <WifiClientEsp32.h>
#include <esp_log.h>
#include <cstring>


using namespace Peripheral;

const char TAG[] = "WifiClient";

WifiClientEsp32::WifiClientEsp32(etl::imessage_timer &timer) : WifiClient(timer) {

}

bool WifiClientEsp32::init() {
//    esp_log_level_set("wifi", ESP_LOG_NONE); // disable wifi driver logging
    tcpip_adapter_init();
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
//    ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &config));
    esp_wifi_get_config(ESP_IF_WIFI_STA, &config);
    ESP_ERROR_CHECK( esp_wifi_start() );
    return true;
}

bool WifiClientEsp32::setSSID(etl::string_view ssid) {
//    strcpy((char *)config.sta.ssid,"Normalny Dom");
    strcpy((char *)config.sta.ssid,ssid.cbegin());
    SSID.assign((char *)config.sta.ssid,ssid.size());
    return false;
}

bool WifiClientEsp32::setPassword(etl::string_view passwd) {
//    std::copy(passwd.cbegin(),passwd.cend(),config.sta.password);
    strcpy((char *)config.sta.password,passwd.cbegin());

    Passwd.assign((char *)config.sta.password,passwd.size());
    return true;

}

etl::string_view WifiClientEsp32::getSSID() {
    return SSID;
}

etl::string_view WifiClientEsp32::getPassword() {
    return Passwd;
}

std::optional<etl::string_view> WifiClientEsp32::getIP(etl::istring & ip_buf){
    tcpip_adapter_ip_info_t ipInfo;
    tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);
    char ip[20];
    sprintf(ip,"%s",ip4addr_ntoa(&ipInfo.ip));
    ip_buf.assign(ip);
    return etl::string_view(ip_buf.cbegin(),ip_buf.size());
}

bool WifiClientEsp32::connect() {

    WifiClient::connect();
//    ESP_ERROR_CHECK(esp_wifi_disconnect());
    printf("esp struct SSID: %s, Passwd: %s\n",config.sta.ssid,config.sta.password);


    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &config));
    ESP_ERROR_CHECK(esp_wifi_connect());
    return true;
}

void WifiClientEsp32::disconnect() {
    esp_wifi_disconnect();
}

bool WifiClientEsp32::isConnected() {
    return Connected;
}


void WifiClientEsp32::onConnected() {
    wifi_ap_record_t info;
    esp_wifi_sta_get_ap_info(&info);
    ESP_LOGI(TAG,"WifiConnected, SSID: %s",info.ssid);
    WifiClient::onConnected();
    Connected = true;
}

void WifiClientEsp32::onDisconnected() {
    ESP_LOGI(TAG,"WifiDisconnected");
    Connected = false;
    WifiClient::onDisconnected();
}
