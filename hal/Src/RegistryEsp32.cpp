//
// Created by mchodzikiewicz on 3/9/19.
//


#include "RegistryEsp32.h"
System::RegistryEsp32::RegistryEsp32() {

}

void System::RegistryEsp32::init() {
    esp_err_t err;
    printf("Opening Non-Volatile Storage (NVS) handle... ");
    err = nvs_open("storage", NVS_READWRITE, &my_handle);
    if (err != ESP_OK) {
        printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    } else {
        printf("Done\n");
    }
}

std::optional<System::Registry::StoredValue> System::RegistryEsp32::getEntry(etl::string_view entry) {
    // Read
    printf("Reading \'%s\' from registry\n",entry.cbegin());

    uint8_t buffer[sizeof(StoredValue)];
    size_t size = sizeof(StoredValue);
    esp_err_t err = nvs_get_blob(my_handle,entry.cbegin(),&buffer,&size);
    if(err != ESP_OK) {
        printf("Failed %x\n",err);
        return std::nullopt;
    } else {
        auto value = reinterpret_cast<StoredValue*>(buffer);
        if(!value->is_valid()) {
            printf("Not valid\n");
            return std::nullopt;
        } else  if(value->is_type<etl::string<32>>()) {
            value->get<etl::string<32>>().repair();
            printf("Retrieved '%s' with '%s' ", entry.cbegin(), value->get<etl::string<32>>().c_str());
        } else {
            printf("Retrieved %s from NVS ... ", entry.cbegin());
        }

        printf("Success\n");

        return *value;
    }

}

bool System::RegistryEsp32::storeEntry(etl::string_view entry, System::Registry::StoredValue value) {
    // Write
    esp_err_t err;
    if(value.is_type<etl::string<32>>()) {
        value.get<etl::string<32>>().repair();
        printf("Updating '%s' with '%s' ", entry.cbegin(), value.get<etl::string<32>>().c_str());
    } else {
        printf("Updating %s in NVS ... ", entry.cbegin());
    }

    size_t size = sizeof(StoredValue);
    err = nvs_set_blob(my_handle, entry.cbegin(), reinterpret_cast<void *>(&value),size);
    if(err == ESP_OK){
        printf("Done\n");
    } else {
        printf("Failed! 0x%x\n", err);
    }


    printf("Committing %s update in NVS ... ",entry.cbegin());
    err = nvs_commit(my_handle);
    if(err == ESP_OK){
        printf("Done\n");
    } else {
        printf("Failed! 0x%x\n", err);
    }
    return err == ESP_OK;
}

bool System::RegistryEsp32::eraseRegistry() {
    printf("Erasing registry!\n");
    nvs_flash_erase();
    return true;
}

bool System::RegistryEsp32::eraseEntry(etl::string_view entry) {
    return nvs_erase_key(my_handle, entry.data()) == ESP_OK;
}




