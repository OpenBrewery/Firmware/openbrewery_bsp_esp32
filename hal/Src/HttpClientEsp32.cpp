//
// Created by mchodzikiewicz on 2/3/19.
//

#include <esp_tls.h>
#include "HttpClientEsp32.h"
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"


#include "esp_log.h"
#include "etl/cstring.h"

//TODO (mchodzikiewicz): cert should be passed via constructor instead of global extern
#include "HttpsCert.h"

/* Constants that aren't configurable in menuconfig */

static const char *TAG = "http client";

static const etl::string_view con_get("GET ");
static const etl::string_view con_post("POST ");

static const etl::string_view con_http(" HTTP/1.0\r\n");
static const etl::string_view con_host("Host: ");
static const etl::string_view con_useragent("\nUser-Agent: OpenBrewery/Beerbob\r\n");
static const etl::string_view con_content_type("Content-Type: application/json;charset=UTF-8\r\n");
static const etl::string_view con_length("Content-Length: ");

static const etl::string_view con_prebody("\r\n");

Communication::HttpResponse parse_response(std::optional<etl::string_view> & response){
    if(response){
        etl::string_view& res = response.value();
        if(res.starts_with("HTTP/1.1 200 OK")) {
            return {200,etl::string_view(&res.at(res.find("\n\n")+2), res.cend())};
        } else {
            //TODO: error code should be returned
            return Communication::HttpResponse{404,std::nullopt};
        }
    } else {
        return Communication::HttpResponse{404,std::nullopt};
    }
}

Communication::HttpResponse Communication::HttpClientEsp32::get(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) {
    etl::string<1024> req;

    auto append = [](etl::istring & req, etl::string_view str){
            req.append(str.cbegin(),str.cend());
        };

    append(req,con_get);
    append(req,target);
    append(req,con_http);
    append(req,con_host);
    append(req,host);
    append(req,con_useragent);
    append(req,AuthString);

    printf("Request:\n%s\n",req.c_str());

    std::optional<etl::string_view> response = request(host,port,etl::string_view(req.c_str(),req.size()));

    if(response) {
        printf("Response:\n%s\n", response.value().data());
    } else {
        printf("Empty response\n");
    }
    return parse_response(response);

}

Communication::HttpResponse
Communication::HttpClientEsp32::post(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) {

    reqBuffer.clear();
    resBuffer.clear();
    auto append = [](etl::istring & req, etl::string_view str){
            req.append(str.cbegin(),str.cend());
        };

    append(reqBuffer,con_post);
    append(reqBuffer,target);
    append(reqBuffer,con_http);
    append(reqBuffer,con_host);
    append(reqBuffer,host);
    append(reqBuffer,con_useragent);
    append(reqBuffer,con_content_type);
    char content_len[10];
    itoa(content.size(),content_len,10);
    append(reqBuffer,con_length);
    append(reqBuffer,content_len);
    append(reqBuffer,"\r\n");
    append(reqBuffer,AuthString);
    append(reqBuffer,content);

    printf("Request:\n%s\n",reqBuffer.c_str());

    std::optional<etl::string_view> response = request(host,port,etl::string_view(reqBuffer.c_str(),reqBuffer.size()));

    if(response) {
        printf("Response:\n%s\n", response.value().data());
    } else {
        printf("Empty response\n");
    }

    return parse_response(response);
}

Communication::HttpResponse
Communication::HttpClientEsp32::Sget(etl::istring &resBuffer, etl::string_view host, int port,
                                     etl::string_view target) {
    etl::string<1024> req;

    auto append = [](etl::istring & req, etl::string_view str){
        req.append(str.cbegin(),str.cend());
    };

    append(req,con_get);
    append(req,target);
    append(req,con_http);
    append(req,con_host);
    append(req,host);
    append(req,con_useragent);
    append(req,AuthString);

    printf("Request:\n%s\n",req.c_str());

    etl::string<128> url;
    url.assign(host.cbegin(),host.cend());
    url.append(target.cbegin(),target.cend());

    std::optional<etl::string_view> response = Srequest(url,port,etl::string_view(req.c_str(),req.size()));

    if(response) {
        printf("Response:\n%s\n", response.value().data());
    } else {
        printf("Empty response\n");
    }
    return parse_response(response);
}

Communication::HttpResponse
Communication::HttpClientEsp32::Spost(etl::istring & reqBuffer, etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target,
                                      etl::string_view content) {

    reqBuffer.clear();
    resBuffer.clear();
    auto append = [](etl::istring & req, etl::string_view str){
        req.append(str.cbegin(),str.cend());
    };

    append(reqBuffer,con_post);
    append(reqBuffer,target);
    append(reqBuffer,con_http);
    append(reqBuffer,con_host);
    append(reqBuffer,host);
    append(reqBuffer,con_useragent);
    append(reqBuffer,AuthString);
    append(reqBuffer,con_content_type);
    char content_len[10];
    itoa(content.size(),content_len,10);
    append(reqBuffer,con_length);
    append(reqBuffer,content_len);
    append(reqBuffer,"\r\n");
    append(reqBuffer,con_prebody);
    append(reqBuffer,content);

    etl::string<512> url;
    url.append("https://");
    url.append(host.cbegin(),host.cend());
    url.append(target.cbegin(),target.cend());

    printf("Posting to %s:\n request:\n%s\n",url.c_str(),reqBuffer.c_str());

    std::optional<etl::string_view> response = Srequest(url,port,etl::string_view(reqBuffer.c_str(),reqBuffer.size()));

    if(response) {
        printf("Response:\n%s\n", response.value().data());
    } else {
        printf("Empty response\n");
    }

    return parse_response(response);
}


void Communication::HttpClientEsp32::setReceiveBuffer(httpBuf buffer) {
    ReceiveBuffer = buffer;
}

std::optional<etl::string_view>
Communication::HttpClientEsp32::request(etl::string_view host, int port, etl::string_view request) {
    struct addrinfo hints;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    struct addrinfo *res;

    struct in_addr *addr;

    int s, r;

    char s_port[6];
    int err = getaddrinfo(host.data(), itoa(port,s_port,10), &hints, &res);
    if(err != 0 || res == NULL) {
        return std::nullopt;
    }

   /* Code to print the resolved IP.
       Note: inet_ntoa is non-reentrant, look at ipaddr_ntoa_r for "real" code */
    addr = &((struct sockaddr_in *)res->ai_addr)->sin_addr;
    ESP_LOGI(TAG, "DNS lookup succeeded. IP=%s", inet_ntoa(*addr));

    s = socket(res->ai_family, res->ai_socktype, 0);
    if(s < 0) {
        ESP_LOGE(TAG, "... Failed to allocate socket.");
        freeaddrinfo(res);
        return std::nullopt;
    }
    ESP_LOGI(TAG, "... allocated socket");

    if(connect(s, res->ai_addr, res->ai_addrlen) != 0) {
        ESP_LOGI(TAG, "... socket connect failed errno=%d", errno);
        close(s);
        freeaddrinfo(res);
        return std::nullopt;

    }

    ESP_LOGI(TAG, "... connected");
    freeaddrinfo(res);

    if(write(s,request.data(),request.size()) < 0){
        ESP_LOGE(TAG, "Socket send failed!");
        close(s);
        return std::nullopt;
    }


    struct timeval receiving_timeout;
    receiving_timeout.tv_sec = 5;
    receiving_timeout.tv_usec = 0;
    if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &receiving_timeout,
                   sizeof(receiving_timeout)) < 0) {
        ESP_LOGE(TAG, "... failed to set socket receiving timeout");
        close(s);
        return std::nullopt;
    }
    ESP_LOGI(TAG, "... set socket receiving timeout success");

    /* Read HTTP response */
    bzero(ReceiveBuffer.buf, ReceiveBuffer.size);
    do {
        r = read(s, ReceiveBuffer.buf, ReceiveBuffer.size-1);
    } while(r > 0);

    ESP_LOGI(TAG, "... done reading from socket. Last read return=%d errno=%d\r\n", r, errno);
    close(s);

    return etl::string_view((char *)ReceiveBuffer.buf,ReceiveBuffer.size);

}

std::optional<etl::string_view>
Communication::HttpClientEsp32::Srequest(etl::string_view url, int port, etl::string_view request) {
    uint32_t free_heap_size=0, min_free_heap_size=0;
    free_heap_size = esp_get_free_heap_size();
    min_free_heap_size = esp_get_minimum_free_heap_size();
    printf("\n free heap size = %d \t  min_free_heap_size = %d \n",free_heap_size,min_free_heap_size);

    static esp_tls_cfg_t cfg;

    int ret;
    size_t readCounter = 0;
    size_t written_bytes = 0;


    cfg.cacert_pem_buf  = cert;
    cfg.cacert_pem_bytes = sizeof(cert);
//    cfg.use_global_ca_store = true;


//    printf("Using cert:\n%s\n",cert);
    struct esp_tls *tls = esp_tls_conn_http_new(url.data(), &cfg);

    if(tls != NULL) {
        ESP_LOGI(TAG, "Connection established...");
    } else {
        ESP_LOGE(TAG, "Connection failed...");
        //TODO: dealocating exception should be introduced
        goto exit;
    }

    do {
        ret = esp_tls_conn_write(tls,
                                 request.data() + written_bytes,
                                 request.size() - written_bytes);
        if (ret >= 0) {
            ESP_LOGI(TAG, "%d bytes written", ret);
            written_bytes += ret;
        } else if (ret != MBEDTLS_ERR_SSL_WANT_READ  && ret != MBEDTLS_ERR_SSL_WANT_WRITE) {
            ESP_LOGE(TAG, "esp_tls_conn_write  returned 0x%x", ret);
            //TODO: dealocating exception should be introduced
            goto exit;
        }
    } while(written_bytes < request.size());

    ESP_LOGI(TAG, "Reading HTTP response...");


    bzero(ReceiveBuffer.buf, ReceiveBuffer.size);


    do
    {
        ret = esp_tls_conn_read(tls, (char *)ReceiveBuffer.buf+readCounter, ReceiveBuffer.size-readCounter);

        if(ret == MBEDTLS_ERR_SSL_WANT_WRITE  || ret == MBEDTLS_ERR_SSL_WANT_READ)
            continue;

        if(ret < 0)
        {
            ESP_LOGE(TAG, "esp_tls_conn_read  returned -0x%x", -ret);
            break;
        }

        if(ret == 0)
        {
            ESP_LOGI(TAG, "connection closed");
            break;
        }

        readCounter += ret;
        ESP_LOGD(TAG, "%d bytes read", ret);
    } while(1);

    ESP_LOGI(TAG, "Received %i bytes",readCounter);
    esp_tls_conn_delete(tls);
    return etl::string_view((char *)ReceiveBuffer.buf,readCounter);


    exit:
        esp_tls_conn_delete(tls);
        putchar('\n'); // JSON output doesn't have a newline at end

        static int request_count;
        ESP_LOGI(TAG, "Completed %d requests", ++request_count);

        for(int countdown = 10; countdown >= 0; countdown--) {
            ESP_LOGI(TAG, "%d...", countdown);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
        ESP_LOGI(TAG, "Starting again!");
        return std::nullopt;
}



void Communication::HttpClientEsp32::setAuthorization(etl::string_view auth_str) {
    AuthString.assign(auth_str.cbegin(),auth_str.cend());
}

