//
// Created by mchodzikiewicz on 2/3/19.
//

#ifndef PROJECT_WIFICLIENTESP32_H
#define PROJECT_WIFICLIENTESP32_H


#include <WifiClient.h>
#include "esp_wifi.h"

namespace Peripheral {
    class WifiClientEsp32 : public WifiClient {
    public:
        explicit  WifiClientEsp32(etl::imessage_timer & timer);

        bool init() override;

        bool setSSID(etl::string_view ssid) override;

        bool setPassword(etl::string_view passwd) override;

        etl::string_view getSSID() override;

        etl::string_view getPassword() override;

        std::optional<etl::string_view> getIP(etl::istring & ip_buf);

        bool connect() override;

        void disconnect() override;

        bool isConnected() override;

        //Callbacks from esp events:
        void onConnected() override;

        void onDisconnected() override;

    private:

        bool Connected;
        wifi_config_t config;
        etl::string_view SSID;
        etl::string_view Passwd;

    };
}


#endif //PROJECT_WIFICLIENTESP32_H
