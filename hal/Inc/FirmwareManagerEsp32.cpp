//
// Created by mchodzikiewicz on 8/6/19.
//

#include <esp_log.h>
#include "FirmwareManagerEsp32.h"
#include "esp_https_ota.h"
//TODO (mchodzikiewicz): cert should be passed via constructor instead of global extern
#include "HttpsCert.h"

bool System::FirmwareManagerEsp32::downloadFirmwarePackage(etl::string_view url) {
    printf("FW MAN: upgrade\n");
    esp_http_client_config_t config = {0};
    config.url = url.data();
    config.cert_pem = (char *)cert;
    config.timeout_ms = 10000;
//    config.buffer_size = 4096;
//    config.event_handler = _http_event_handler;
    esp_err_t ret = esp_https_ota(&config);
    if (ret == ESP_OK) {
        printf("FOTA success\n");
        esp_restart();
    } else {
        ESP_LOGE("FOTA", "Firmware upgrade failed");
    }
    return false;
}
