#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

#include "I2CInterfaceEsp32.h"

/* Can run 'make menuconfig' to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO GPIO_NUM_2


class Led {
public:
    void off(){
        gpio_set_level(BLINK_GPIO, 0);
    }
    void on(){
        gpio_set_level(BLINK_GPIO, 1);
    }
};


void blink_task(void *pvParameter)
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    Led led;
    gpio_pad_select_gpio(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);


    Peripheral::I2CInterfaceEsp32 i2c(Peripheral::I2CInterface::Config::Mode::Master, 150000);

    i2c.open();

    while(1) {
        /* Blink off (output low) */
        led.off();
        vTaskDelay(100 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        led.on();
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}


extern "C" {

void app_main() {
    xTaskCreate(&blink_task, "blink_task", configMINIMAL_STACK_SIZE, NULL, 0, NULL);
    while (1);
}

}