//
// Created by mchodzikiewicz on 3/9/19.
//

#ifndef OPENBREWERY_BSP_ESP32_REGISTRYESP32_H
#define OPENBREWERY_BSP_ESP32_REGISTRYESP32_H
#include "Registry.h"
#include <nvs.h>
#include <nvs_flash.h>


namespace System {
    class RegistryEsp32 : public Registry {
    public:
        RegistryEsp32();

        bool eraseEntry(etl::string_view entry) override;

        void init();

        std::optional<StoredValue> getEntry(etl::string_view entry) override;

        bool storeEntry(etl::string_view entry, StoredValue value) override;

        bool eraseRegistry() override;

    private:
        nvs_handle my_handle;
        enum class Type : uint8_t {
            u8, i32, u32, f32, string
        };
    };
}


#endif //OPENBREWERY_BSP_ESP32_REGISTRYESP32_H
