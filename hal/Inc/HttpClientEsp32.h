//
// Created by mchodzikiewicz on 2/3/19.
//

#ifndef PROJECT_HTTPCLIENTESP32_H
#define PROJECT_HTTPCLIENTESP32_H

#include "HttpClient.h"
#include <memory>
#include "etl/array.h"

namespace Communication {


    class HttpClientEsp32 : public HttpClient {

    public:
        HttpResponse get(etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target) override;

        HttpResponse post(etl::istring & reqBuffer, etl::istring & resBuffer, etl::string_view host, int port, etl::string_view target, etl::string_view content) override;

        void setAuthorization(etl::string_view auth_str) override;

        //TODO: refactor to etl::istring
        struct httpBuf {
            uint8_t * buf;
            size_t size;
        };
        void setReceiveBuffer(httpBuf buffer);

        HttpResponse Sget(etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target) override;

        HttpResponse Spost(etl::istring & reqBuffer, etl::istring &resBuffer, etl::string_view host, int port, etl::string_view target,
                           etl::string_view content) override;

    private:
        std::optional<etl::string_view> request(etl::string_view host, int port, etl::string_view request);
        std::optional<etl::string_view> Srequest(etl::string_view url, int port, etl::string_view request);
        //TODO: pass it as parameter for each call
        httpBuf ReceiveBuffer;
        etl::string<123> AuthString;
    };
}

#endif //PROJECT_HTTPCLIENTESP32_H
