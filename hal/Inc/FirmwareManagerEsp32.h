//
// Created by mchodzikiewicz on 8/6/19.
//

#ifndef OPENBREWERY_BSP_ESP32_FIRMWAREMANAGERESP32_H
#define OPENBREWERY_BSP_ESP32_FIRMWAREMANAGERESP32_H

#include <FirmwareManager.h>

namespace System {
    class FirmwareManagerEsp32 : public FirmwareManager {
    public:
        bool downloadFirmwarePackage(etl::string_view url) override;
    };
}


#endif //OPENBREWERY_BSP_ESP32_FIRMWAREMANAGERESP32_H
