//
// Created by mchodzikiewicz on 2/15/19.
//

#include "WifiServerEsp32.h"
#include <esp_log.h>
#include <WifiServerEsp32.h>
#include <cstring>


bool Peripheral::WifiServerEsp32::init() {
//    esp_log_level_set("wifi", ESP_LOG_NONE); // disable wifi driver logging
    tcpip_adapter_init();
    init_config = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&init_config) );
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_AP) );

    ESP_ERROR_CHECK(tcpip_adapter_dhcps_stop(TCPIP_ADAPTER_IF_AP));
    // assign a static IP to the network interface
    tcpip_adapter_ip_info_t info;
    memset(&info, 0, sizeof(info));
    IP4_ADDR(&info.ip, 192, 168, 1, 1);
    IP4_ADDR(&info.gw, 192, 168, 1, 1);//ESP acts as router, so gw addr will be its own addr
    IP4_ADDR(&info.netmask, 255, 255, 255, 0);
    ESP_ERROR_CHECK(tcpip_adapter_set_ip_info(TCPIP_ADAPTER_IF_AP, &info));
    // start the DHCP server
    ESP_ERROR_CHECK(tcpip_adapter_dhcps_start(TCPIP_ADAPTER_IF_AP));
    printf("DHCP server started \n");

    return true;
}

bool Peripheral::WifiServerEsp32::setSSID(etl::string_view ssid) {
    std::copy(ssid.cbegin(),ssid.cend(),config.ap.ssid);
    config.ap.ssid_len = static_cast<uint8_t>(ssid.size());
    SSID.assign((char *)config.ap.ssid,ssid.size());
    return true;
}

bool Peripheral::WifiServerEsp32::setPassword(etl::string_view passwd) {
    strcpy((char *)config.ap.password,passwd.cbegin());

//        std::copy(passwd.cbegin(),passwd.cend(),config.ap.password);
    Passwd.assign((char *)config.ap.password,passwd.size());
    return true;

}

etl::string_view Peripheral::WifiServerEsp32::getSSID() {
    return SSID;
}

etl::string_view Peripheral::WifiServerEsp32::getPassword() {
    return Passwd;
}

bool Peripheral::WifiServerEsp32::serve() {
//    ESP_ERROR_CHECK(esp_wifi_disconnect());
    config.ap.max_connection = MaxClients;
    config.ap.authmode = WIFI_AUTH_WPA2_PSK;
    config.ap.channel = 0;
    config.ap.beacon_interval = 200;
    config.ap.ssid_hidden = 0;
    wifi_country_t country;
    sprintf(country.cc, "PL");
    country.schan = 1;
    country.nchan = 13;
    country.policy = WIFI_COUNTRY_POLICY_AUTO;
    esp_wifi_set_country(&country);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_AP) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &config));
    ESP_ERROR_CHECK(esp_wifi_start());

    return true;
}

void Peripheral::WifiServerEsp32::stop() {
    esp_wifi_stop();
}

bool Peripheral::WifiServerEsp32::isServing() {
    return false;
}

void Peripheral::WifiServerEsp32::setMaxClients(size_t maxClients) {
    MaxClients = maxClients;
}
