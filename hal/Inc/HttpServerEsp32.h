//
// Created by mchodzikiewicz on 2/24/19.
//

#ifndef OPENBREWERY_BSP_ESP32_HTTPSERVERESP32_H
#define OPENBREWERY_BSP_ESP32_HTTPSERVERESP32_H


#include <HttpServer.h>
#include <esp_http_server.h>
#include "etl/flat_map.h"

//TODO: find the way to allow creating multiple server instances... right now it is singleton due to underlying API

namespace Communication {

    class HttpServerEsp32 : public HttpServer {
    public:
        using GetMap = etl::iflat_map<etl::string_view,std::function<HttpResponse(etl::istring&)>>;
        using PostMap = etl::iflat_map<etl::string_view,std::function<HttpResponse(etl::istring&, etl::string_view)>>;

        //TODO: this method should be constructor instead, and maps should be references
        HttpServerEsp32(GetMap & getMap, PostMap & postMap, etl::istring & resBuffer);


        bool serve(etl::string_view host, uint16_t port) override;

        bool isServing() override;

        bool addGetResource(etl::string_view path, std::function<HttpResponse(etl::istring &)> function) override;

        bool addPostResource(etl::string_view path,
                             std::function<HttpResponse(etl::istring &, etl::string_view)> function) override;

        bool removeGetResource(etl::string_view path) override;

        bool removePostResource(etl::string_view path) override;

        void stop() override;

    private:

        static GetMap * getResourceMap;
        static PostMap * postResourceMap;
        static etl::istring * ResBuffer;


        static esp_err_t getHandler(httpd_req_t *req);
        static esp_err_t postHandler(httpd_req_t * req);



    private:
        httpd_handle_t server;

        bool Serving = false;

    };
}

#endif //OPENBREWERY_BSP_ESP32_HTTPSERVERESP32_H
