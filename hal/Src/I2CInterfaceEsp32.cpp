//
// Created by mchodzikiewicz on 04.12.18.
//


#include "I2CInterfaceEsp32.h"

#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */

bool Peripheral::I2CInterfaceEsp32::open() {
    switch(Config.mode) {
        case I2C_MODE_MASTER:
            ESP_ERROR_CHECK(i2c_param_config(Port, &Config));
            ESP_ERROR_CHECK(i2c_driver_install(Port, Config.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));
//            I2cDevice = i2c_cmd_link_create();
            break;
        case I2C_MODE_SLAVE:
            //TODO: to be implemented
            break;
        default:
            break;
    }
    return false;
}

void Peripheral::I2CInterfaceEsp32::close() {
//    i2c_cmd_link_delete(I2cDevice);
}

bool Peripheral::I2CInterfaceEsp32::isOpened() const {
    return false;
}



bool Peripheral::I2CInterfaceEsp32::setConfig(Peripheral::I2CInterface::Config::Mode mode, int frequency) {
    return false;
}

Peripheral::I2CInterfaceEsp32::I2CInterfaceEsp32(i2c_port_t i2cnum, gpio_num_t sda, gpio_num_t scl,
                                                 Peripheral::I2CInterface::Config::Mode mode, int freq) : I2CInterface(mode,freq), Port(i2cnum) {
    Config.mode = (mode == Config::Mode::Master) ? I2C_MODE_MASTER : I2C_MODE_SLAVE;
    Config.sda_io_num = sda;
    Config.scl_io_num = scl;
    Config.sda_pullup_en = GPIO_PULLUP_DISABLE;
    Config.scl_pullup_en = GPIO_PULLUP_DISABLE;
    Config.master.clk_speed = (uint32_t)freq;

}

bool Peripheral::I2CInterfaceEsp32::__write(const etl::array_view<uint8_t> data) {
    I2cDevice = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(I2cDevice));
    ESP_ERROR_CHECK(i2c_master_write_byte(I2cDevice,*data.begin()<< 1 | I2C_MASTER_WRITE,true)); //Slave addr
    ESP_ERROR_CHECK(i2c_master_write(I2cDevice,(uint8_t *)data.begin()+1,data.size()-1,true));
    i2c_master_stop(I2cDevice);
    i2c_master_cmd_begin(Port, I2cDevice, 50 / portTICK_RATE_MS);
    i2c_cmd_link_delete(I2cDevice);
    return true;
}


size_t Peripheral::I2CInterfaceEsp32::__request(const etl::array_view<uint8_t> request, etl::ivector<uint8_t> &response,
                                                const size_t max_response_size) {
    I2cDevice = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(I2cDevice));
    ESP_ERROR_CHECK(i2c_master_write_byte(I2cDevice,*request.begin()<<1 | I2C_MASTER_WRITE,true)); //Slave addr
    ESP_ERROR_CHECK(i2c_master_write(I2cDevice,(uint8_t *)request.begin()+1,request.size()-1,true));
    ESP_ERROR_CHECK(i2c_master_start(I2cDevice));
    ESP_ERROR_CHECK(i2c_master_write_byte(I2cDevice,*request.begin()<<1 | I2C_MASTER_READ,true));

    //TODO (mchodzikiewicz): this copy is temporary, until proper incoming data container is introduced
    uint8_t buffer[32];
    for(int i=0;i<sizeof(buffer);i++){
        buffer[i] = 0;
    }
    ESP_ERROR_CHECK(i2c_master_read(I2cDevice,buffer,max_response_size,I2C_MASTER_LAST_NACK));

    i2c_master_stop(I2cDevice);
    i2c_master_cmd_begin(Port, I2cDevice, 50 / portTICK_RATE_MS);
    i2c_cmd_link_delete(I2cDevice);
    etl::array_view<uint8_t> res(buffer,max_response_size);
    response.assign(res.cbegin(),res.cend());
    return true;
}



