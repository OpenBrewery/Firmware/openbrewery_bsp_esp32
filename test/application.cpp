#include <string.h>
#include <Registry.h>
#include <RegistryEsp32.h>
#include <SystemEsp32.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "WifiClientEsp32.h"
#include "WifiServerEsp32.h"
#include "Wifi.h"
#include "HttpClientEsp32.h"
#include "HttpServerEsp32.h"
#include "I2CInterfaceEsp32.h"
#include "SHT21.h"
#include "MMA8452.h"
#include "SystemEsp32.h"

//#include "private.h"
//#include "BeerBob.h"


static EventGroupHandle_t wifi_event_group;
const int CONNECTED_BIT = BIT0;

static const char *TAG = "example";


etl::message_timer<64> MessageTimer;

System::RegistryEsp32 registry;

Peripheral::WifiClientEsp32 wifiClient(MessageTimer);
Peripheral::WifiServerEsp32 wifiServer(MessageTimer);
Peripheral::Wifi wifi(std::move(wifiClient),std::move(wifiServer));

uint8_t recv_buf[8192];


etl::string<4096> httpServerResBuffer;
etl::flat_map<etl::string_view,std::function<Communication::HttpResponse(etl::istring&)>,10> GetMap;
etl::flat_map<etl::string_view,std::function<Communication::HttpResponse(etl::istring&, etl::string_view)>,10> PostMap;

Communication::HttpClientEsp32 httpClient;
Communication::HttpServerEsp32 httpServer(GetMap,PostMap,httpServerResBuffer);


Peripheral::I2CInterfaceEsp32 i2c(I2C_NUM_1,GPIO_NUM_0,GPIO_NUM_4,Peripheral::I2CInterface::Config::Mode::Master,10000);

etl::vector<uint8_t,32> acc_i2c_wbuffer;
etl::vector<uint8_t,32> acc_i2c_rbuffer;
Sensor::MMA8452 accelerometer(i2c,0x1c,acc_i2c_wbuffer,acc_i2c_rbuffer);


etl::vector<uint8_t,32> sht_i2c_wbuffer;
etl::vector<uint8_t,32> sht_i2c_rbuffer;
Sensor::SHT21 sht21(i2c,0x40,sht_i2c_wbuffer,sht_i2c_rbuffer);

//BeerBob bob(wifi, accelerometer, sht21.thermometer, sht21.humiditySensor,httpClient,httpServer);

Communication::HttpClientEsp32::httpBuf buffer{recv_buf, sizeof(recv_buf)};

System::SystemEsp32 sys(registry);

static void initialize_nvs()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK( nvs_flash_erase() );
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
        case SYSTEM_EVENT_STA_START:
            wifiClient.onInitialized();
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            wifiClient.onConnected();
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            wifiClient.onDisconnected();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            break;

        case SYSTEM_EVENT_AP_STACONNECTED:
            ESP_LOGI(TAG, "station:"MACSTR" join, AID=%d",
                     MAC2STR(event->event_info.sta_connected.mac),
                     event->event_info.sta_connected.aid);
            break;
        case SYSTEM_EVENT_AP_STADISCONNECTED:
            ESP_LOGI(TAG, "station:"MACSTR"leave, AID=%d",
                     MAC2STR(event->event_info.sta_disconnected.mac),
                     event->event_info.sta_disconnected.aid);
            break;
        default:
            break;
    }
    return ESP_OK;
}

extern "C" {
void app_main() {
    initialize_nvs();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
    wifi_event_group = xEventGroupCreate();

    httpClient.setReceiveBuffer(buffer);
//    httpServer.setMaps(GetMap,PostMap);
    i2c.open();
    accelerometer.init();
    sht21.init();

    printf("Starting Beerbob %s\n%s\n"
            ,sys.getDeviceId().data()
            ,sys.version().data()
    );

//    bob.run();

    while(1);
    sys.reboot();
}

}