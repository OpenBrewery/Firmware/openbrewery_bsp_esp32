//
// Created by mchodzikiewicz on 04.12.18.
//

#ifndef BEERBOB_FW_ESP32_I2CINTERFACEESP32_H
#define BEERBOB_FW_ESP32_I2CINTERFACEESP32_H

#include <driver/i2c.h>
#include "I2CInterface.h"

namespace Peripheral {

    class I2CInterfaceEsp32 : public I2CInterface {
    public:
        I2CInterfaceEsp32(i2c_port_t i2cnum, gpio_num_t sda, gpio_num_t scl, Config::Mode mode, int freq);

        bool open() override;

        void close() override;

        bool isOpened() const override;

    protected:
        bool __write(const etl::array_view<uint8_t> data) override;

        size_t __request(const etl::array_view<uint8_t> request, etl::ivector<uint8_t> &response,
                         const size_t max_response_size) override;

        bool setConfig(Config::Mode mode, int frequency) override;

    private:
        i2c_config_t Config;
        i2c_port_t Port;
        i2c_cmd_handle_t I2cDevice;
    };

}
#endif //BEERBOB_FW_ESP32_I2CINTERFACEESP32_H
