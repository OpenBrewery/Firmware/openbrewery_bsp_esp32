//
// Created by mchodzikiewicz on 2/15/19.
//

#ifndef OPENBREWERY_BSP_ESP32_WIFISERVERESP32_H
#define OPENBREWERY_BSP_ESP32_WIFISERVERESP32_H

#include <WifiServer.h>
#include "esp_wifi.h"

namespace Peripheral {
    class WifiServerEsp32 : public WifiServer {
    public:
        WifiServerEsp32(etl::imessage_timer & timer) : WifiServer(timer), MaxClients(1) {}

        bool init() override;

        bool setSSID(etl::string_view ssid) override;

        bool setPassword(etl::string_view passwd) override;

        etl::string_view getSSID() override;

        etl::string_view getPassword() override;

        bool serve() override;

        void stop() override;

        bool isServing() override;

        void setMaxClients(size_t maxClients);

    private:
        size_t MaxClients;
        wifi_init_config_t init_config;
        wifi_config_t config;
        etl::string_view SSID;
        etl::string_view Passwd;
    };
}

#endif //OPENBREWERY_BSP_ESP32_WIFISERVERESP32_H
