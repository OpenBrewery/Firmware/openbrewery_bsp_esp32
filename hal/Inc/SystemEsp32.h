//
// Created by mchodzikiewicz on 3/10/19.
//

#ifndef OPENBREWERY_BSP_ESP32_SYSTEMESP32_H
#define OPENBREWERY_BSP_ESP32_SYSTEMESP32_H

#include <System.h>
#include <Registry.h>
#include "etl/cstring.h"


namespace System {

    class SystemEsp32 : public System {
    public:
        explicit SystemEsp32(Registry & reg);

        void init();

        etl::string_view getDeviceId() override;

        Version version() override;

        void reboot() override;

        void deepSleep() override;

        float batteryVoltage() override;

        void setClock(std::chrono::time_point<std::chrono::system_clock> timePoint) override;

        void deepSleep(std::chrono::milliseconds sleepTime) override;

        void syncClock() override;

        void delay(std::chrono::milliseconds time) override;

        std::chrono::milliseconds uptime() override;

        std::optional<std::chrono::milliseconds> previousUptime() override;

        std::chrono::time_point<std::chrono::system_clock> now() override;

    private:
        Registry & Reg;
        etl::string<32> uniqueID;
        float LastBatReadout = 0;
    };
}

#endif //OPENBREWERY_BSP_ESP32_SYSTEMESP32_H
