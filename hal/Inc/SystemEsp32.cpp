//
// Created by mchodzikiewicz on 3/10/19.
//

#include <esp_system.h>
#include <thread>
#include "esp_sntp.h"
#include <driver/adc.h>
#include "SystemEsp32.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_timer.h"

#ifndef VERSION_STRING
#define VERSION_STRING "Local build " __DATE__ " " __TIME__
#endif

static const char *TAG = "System";


System::SystemEsp32::SystemEsp32(Registry & reg) : Reg(reg)
{
    batteryVoltage();
}

void System::SystemEsp32::init() {
    uint8_t chipid[6];
    esp_efuse_mac_get_default(chipid);
    uniqueID.resize(uniqueID.max_size());
    uniqueID.resize(snprintf(uniqueID.data(),uniqueID.max_size(),"%02x%02x%02x%02x%02x%02x",chipid[0], chipid[1], chipid[2], chipid[3], chipid[4], chipid[5]));
}



etl::string_view System::SystemEsp32::getDeviceId() {
    return uniqueID;
}

void System::SystemEsp32::reboot() {
    printf("Rebooting!\n");
    esp_restart();
}

void System::SystemEsp32::deepSleep() {
    printf("Entering deep sleep\n");
    esp_deep_sleep_start();
}

void System::SystemEsp32::deepSleep(std::chrono::milliseconds sleepTime) {
    using namespace std::chrono_literals;
    printf("Entering deep sleep for %ims\n", static_cast<int>(sleepTime.count()));
    Reg.storeEntry("wakeupTime",std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::microseconds(esp_timer_get_time())));
    std::this_thread::sleep_for(100ms);

    esp_deep_sleep(std::chrono::duration_cast<std::chrono::microseconds>(sleepTime).count());
}

std::chrono::time_point<std::chrono::system_clock> System::SystemEsp32::now() {
    return std::chrono::system_clock::now();
}

void System::SystemEsp32::setClock(std::chrono::time_point<std::chrono::system_clock> timePoint) {

}

void System::SystemEsp32::syncClock() {
    using namespace std::literals;
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();

    int retry = 0;
    constexpr int retry_count = 20;

    if(now() > std::chrono::system_clock::from_time_t(1600023190)) {
        printf("Time already synchronized!\n");
        return;
    }
    for(int retry = 0; retry < retry_count && now() < std::chrono::system_clock::from_time_t(1600023190); retry ++) {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        std::this_thread::sleep_for(500ms);
        return;
    }
    printf("Failed to synchronize time, going to sleep, will retry after 30s\n");
    deepSleep(30s);
}

float System::SystemEsp32::batteryVoltage() {
    int read_raw;

    esp_err_t r = adc2_get_raw(ADC2_CHANNEL_8, ADC_WIDTH_12Bit, &read_raw);
    if (r == ESP_OK) {
        //1.7961 works in single point, but to make it more accurate, more specific function needs to be provided
        LastBatReadout = (float)read_raw/1000 * 1.7961f;
        printf("Battery: %f\n", LastBatReadout);
    } else if (r == ESP_ERR_TIMEOUT) {
        printf("Battery: ADC2 used by Wi-Fi. Last readout: %f\n", LastBatReadout);
    }
    return LastBatReadout;
}

std::chrono::milliseconds System::SystemEsp32::uptime() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::microseconds(esp_timer_get_time()));
}

std::optional<std::chrono::milliseconds> System::SystemEsp32::previousUptime() {
    using namespace std::chrono_literals;
    auto wakeupTime = Reg.getEntry("wakeupTime");
    return wakeupTime ? wakeupTime.value().get<std::chrono::milliseconds>() : 0ms;
}

System::System::Version System::SystemEsp32::version() {
    return etl::string_view(VERSION_STRING);
}

void System::SystemEsp32::delay(std::chrono::milliseconds time) {
    std::this_thread::sleep_for(time);
}


