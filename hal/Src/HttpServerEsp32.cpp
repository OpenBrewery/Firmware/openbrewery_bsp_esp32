//
// Created by mchodzikiewicz on 2/24/19.
//

#include <HttpServerEsp32.h>
#include <esp_log.h>
#include <algorithm>

static const char *TAG="HttpServer";


Communication::HttpServerEsp32::GetMap * Communication::HttpServerEsp32::getResourceMap;
Communication::HttpServerEsp32::PostMap * Communication::HttpServerEsp32::postResourceMap;
etl::istring * Communication::HttpServerEsp32::ResBuffer;




Communication::HttpServerEsp32::HttpServerEsp32(Communication::HttpServerEsp32::GetMap &getMap,
                                                    Communication::HttpServerEsp32::PostMap &postMap,
                                                    etl::istring &resBuffer) {
    getResourceMap = &getMap;
    postResourceMap = &postMap;
    ResBuffer = &resBuffer;
}


bool Communication::HttpServerEsp32::serve(etl::string_view host, uint16_t port) {
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.uri_match_fn = httpd_uri_match_wildcard;
    server = NULL;


    config.server_port = port;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    httpd_uri_t uri_get;
    uri_get.uri       = "*";
    uri_get.method    = HTTP_GET;
    uri_get.handler   = getHandler;
    uri_get.user_ctx = (void*)"Get";

    httpd_uri_t uri_post;
    uri_post.uri       = "*";
    uri_post.method    = HTTP_POST;
    uri_post.handler   = postHandler;
    uri_post.user_ctx = (void*)"Post";


    auto res =  httpd_start(&server, &config);
    if (res == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &uri_get);
        httpd_register_uri_handler(server, &uri_post);
        return true;
    }

    ESP_LOGI(TAG, "Error starting server! %x",res);
    //TODO (mchodzikiewicz): this info should be retrieved from httpd
    Serving = true;
    return false;
}

bool Communication::HttpServerEsp32::addGetResource(etl::string_view path,
                                                    std::function<HttpResponse(etl::istring &)> function) {
    getResourceMap->insert({path, function});
    return true;
}

bool Communication::HttpServerEsp32::addPostResource(etl::string_view path, std::function<HttpResponse(etl::istring &,
                                                                                                         etl::string_view)> function) {
    postResourceMap->insert({path, function});
    return true;
}

void Communication::HttpServerEsp32::stop() {
    httpd_stop(server);

    //TODO (mchodzikiewicz): this info should be retrieved from httpd
    Serving = false;
}

extern "C" {
esp_err_t Communication::HttpServerEsp32::getHandler(httpd_req_t *req) {
    char *buf;
    size_t buf_len;

    /* Get header value string length and allocate memory for length + 1,
     * extra byte for null termination */
    buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
    if (buf_len > 1) {
        buf = (char *) malloc(buf_len);
        /* Copy null terminated value string into buffer */
        if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found header => Host: %s", buf);
        }
        free(buf);
    }

    buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-2") + 1;
    if (buf_len > 1) {
        buf = (char *) malloc(buf_len);
        if (httpd_req_get_hdr_value_str(req, "Test-Header-2", buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found header => Test-Header-2: %s", buf);
        }
        free(buf);
    }

    buf_len = httpd_req_get_hdr_value_len(req, "Test-Header-1") + 1;
    if (buf_len > 1) {
        buf = (char *) malloc(buf_len);
        if (httpd_req_get_hdr_value_str(req, "Test-Header-1", buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found header => Test-Header-1: %s", buf);
        }
        free(buf);
    }

    /* Read URL query string length and allocate memory for length + 1,
     * extra byte for null termination */
    buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1) {
        buf = (char *) malloc(buf_len);
        if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
            ESP_LOGI(TAG, "Found URL query => %s", buf);
            char param[32];
            /* Get value of expected key from query string */
            if (httpd_query_key_value(buf, "query1", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found URL query parameter => query1=%s", param);
            }
            if (httpd_query_key_value(buf, "query3", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found URL query parameter => query3=%s", param);
            }
            if (httpd_query_key_value(buf, "query2", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found URL query parameter => query2=%s", param);
            }
        }
        free(buf);
    }

    /* Set some custom headers */
    httpd_resp_set_hdr(req, "Custom-Header-1", "Custom-Value-1");
    httpd_resp_set_hdr(req, "Custom-Header-2", "Custom-Value-2");

    /* Send response with custom headers and body set as the
     * string passed in user context*/
    Communication::HttpResponse httpResponse;
    auto resource = getResourceMap->find(etl::string_view(req->uri, strnlen(req->uri, 512)));
    if (resource != getResourceMap->cend()) {
        httpResponse = (*getResourceMap->find(etl::string_view(req->uri, strnlen(req->uri, 512)))).second(*ResBuffer);
    } else {
        ResBuffer->assign("Resource not found");
    }

    ESP_LOGI(TAG, "=========== ACCESSED URL ===========");
    ESP_LOGI(TAG, "%s", req->uri);
    ESP_LOGI(TAG, "====================================");

    ESP_LOGI(TAG, "=========== REPLIED DATA ===========");
    ESP_LOGI(TAG, "%s", httpResponse.Response.value().cbegin());
    ESP_LOGI(TAG, "====================================");
    httpd_resp_send(req, httpResponse.Response.value().cbegin(), httpResponse.Response.value().size());

    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    if (httpd_req_get_hdr_value_len(req, "Host") == 0) {
        ESP_LOGI(TAG, "Request headers lost");
    }
    return ESP_OK;
}


esp_err_t Communication::HttpServerEsp32::postHandler(httpd_req_t *req) {
    char buf[100];
    int ret;
    size_t remaining = req->content_len;

    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, buf,
                                  std::min(remaining, sizeof(buf)))) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }

        Communication::HttpResponse httpResponse;
        auto resource = postResourceMap->find(etl::string_view(req->uri, strnlen(req->uri, 512)));
        if (resource != postResourceMap->cend()) {
            httpResponse = (*postResourceMap->find(etl::string_view(req->uri, strnlen(req->uri, 512)))).second(*ResBuffer,etl::string_view(buf,req->content_len));
        } else {
            ResBuffer->assign("Resource not found");
        }
        httpd_resp_send_chunk(req, httpResponse.Response.value().cbegin(), httpResponse.Response.value().size());
        remaining -= ret;

        /* Log data received */
        ESP_LOGI(TAG, "=========== RECEIVED DATA ==========");
        ESP_LOGI(TAG, "%.*s", ret, buf);
        ESP_LOGI(TAG, "====================================");

        ESP_LOGI(TAG, "=========== REPLIED DATA ===========");
        ESP_LOGI(TAG, "%s", httpResponse.Response.value().cbegin());
        ESP_LOGI(TAG, "====================================");


    }

    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;
}
}
bool Communication::HttpServerEsp32::isServing() {
    return Serving;
}

bool Communication::HttpServerEsp32::removeGetResource(etl::string_view path) {
    auto queryHandler = postResourceMap->find(path);
    if(queryHandler != postResourceMap->cend()){
        postResourceMap->erase(queryHandler);
        return true;
    }
    return false;
}

bool Communication::HttpServerEsp32::removePostResource(etl::string_view path) {
    auto queryHandler = getResourceMap->find(path);
    if(queryHandler != getResourceMap->cend()){
        getResourceMap->erase(queryHandler);
        return true;
    }
    return false;
}
